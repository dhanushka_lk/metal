﻿$(document).ready(function () {
    window.setInterval(function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(success);
        } else {
            alert("Geo Location is not supported on your current browser!");
        }
    }, 5000);

    function success(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        var url = "/Home/StoreLocation/";

        $.ajax({
            url: url,
            data: { lat: lat,lan:lng },
            cache: false,
            type: "POST",
            success: function (data) {
            },
            error: function (reponse) {
            }
        });
    }

});