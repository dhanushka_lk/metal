﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yihui.Common
{
    public enum UserRoles
    {
        SuperAdmin,

        Admin,

        Internal,

        BayWorker
    }

    public enum Status
    {
        Not_Started = 1,

        InProgress = 2,

        Completed = 3,

        On_Hold = 4,

        Cancelled = 5,

        Sorted=6,

        NotSorted=7
    }
}