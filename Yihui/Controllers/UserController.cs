﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using PagedList;
using System.Web.Configuration;
using WebMatrix.WebData;
using System.Web.Security;

namespace Yihui.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /User/
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Index()
        {
            string page = string.Empty;
            var queryString = Request.QueryString.GetValues("grid-page");
            if (queryString != null && queryString.Count() > 0)
            {
                page = queryString.FirstOrDefault();
                ViewBag.Page = page;
            }

            //string userRole = Common.UserRoles.Internal.ToString();

            //var result = (from userRoles in db.UsersInRoles
            //              join role in db.Roles on userRoles.RoleId equals role.RoleId
            //              join user in db.Users on userRoles.UserId equals user.UserId
            //              where role.RoleName != userRole && user.Deleted!=true
            //              select user).ToList();

            return View(db.Users.Where(u=>u.Deleted==false && u.UserId!=1).ToList());
        }

        //
        // GET: /User/Details/5
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Details(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [Authorize(Roles = "Internal")]
        public ActionResult MyProfile()
        {
            var userId = WebSecurity.GetUserId(User.Identity.Name);
            User user = db.Users.Find(userId);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // GET: /User/Create
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Create()
        {
            List<Role> userRoles = db.Roles.Where(r => r.RoleId != 1).ToList();
            ViewBag.UserRoles = userRoles;
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName");
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                var userId = WebSecurity.GetUserId(User.Identity.Name);
 
                WebSecurity.CreateUserAndAccount(user.UserName, "123456", propertyValues: new
                {
                    FName=user.FName,
                    LName=user.LName,
                    IsActive=true,
                    DeptId=user.DeptId,
                    DateOfJoin=DateTime.Now,
                    NICNo=user.NICNo,
                    Address = user.Address,
                    ContactNo = user.ContactNo,
                    Email = user.Email,
                    Deleted = false,
                    CreatedDate = DateTime.Now,
                    CreatedBy = userId,
                });

                List<Role> userRoles = db.Roles.ToList();
                string roleName = string.Empty;
                foreach (Role role in userRoles)
                {
                    var result = Request[Convert.ToString(role.RoleId)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            roleName = role.RoleName;
                        }
                    }

                }
                if (!string.IsNullOrEmpty(roleName))
                {
                    if (!Roles.RoleExists(roleName))
                        Roles.CreateRole(roleName);
                    Roles.AddUserToRole(user.UserName, roleName);
                }

                return RedirectToAction("Index");
            }

            ViewBag.UserRoles = db.Roles.Where(r => r.RoleId != 1).ToList();
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", user.DeptId);
            return View(user);
        }

        //
        // GET: /User/Edit/5
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Edit(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", user.DeptId);
            return View(user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Edit(User user)
        {
            if (ModelState.IsValid)
            {
                user.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                user.ModifiedBy = userId;

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", user.DeptId);
            return View(user);
        }

        //
        // GET: /User/Edit/5

        [Authorize(Roles = "Internal")]
        public ActionResult UpdateProfile(int id = 0)
        {
            var userId = WebSecurity.GetUserId(User.Identity.Name);
            User user = db.Users.Find(userId);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", user.DeptId);
            return View(user);
        }

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult UpdateProfile(User user)
        {
            if (ModelState.IsValid)
            {
                user.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                user.ModifiedBy = userId;

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MyProfile");
            }
            ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", user.DeptId);
            return View(user);
        }

        //
        // GET: /User/Delete/5
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult Delete(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            user.Deleted = true;
            db.Entry(user).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}