﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using Yihui.DAL;

namespace Yihui.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        public ActionResult Index()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperAdmin"))
            {
                return RedirectToAction("SystemDashBoard");
            }
            else if (User.IsInRole(Common.UserRoles.Internal.ToString()))
            {
                return RedirectToAction("Index","InternalAssignment");
            }
            else
            {
                return RedirectToAction("Index", "InternalWeigingTask");
            }
        }

        [Authorize(Roles = "Admin,SuperAdmin")]
        public ActionResult SystemDashBoard()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StoreLocation(string lat, string lan)
        {
            if (!string.IsNullOrEmpty(lat) && !string.IsNullOrEmpty(lan))
            {
                if (User.IsInRole(Common.UserRoles.Internal.ToString()))
                {
                    var userId = WebSecurity.GetUserId(User.Identity.Name);
                    var session = Session["device"];
                    if (session != null)
                    {
                        int deviceId = Convert.ToInt32(Session["device"]);
                        var result = db.UserDevices.Where(ud => ud.UserId == userId && ud.DeviceId == deviceId && ud.LoggedIn == true).FirstOrDefault();
                        if (result != null)
                        {
                            result.Lat = lat;
                            result.Lan = lan;
                            db.Entry(result).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
