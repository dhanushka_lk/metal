﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class VehicleController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Vehicle/

        public ActionResult Index()
        {
            string page = string.Empty;
            var queryString = Request.QueryString.GetValues("grid-page");
            if (queryString != null && queryString.Count() > 0)
            {
                page = queryString.FirstOrDefault();
                ViewBag.Page = page;
            }

            var vehicles = from t in db.Vehicles
                            where t.Deleted != true
                            select t;

            return View(vehicles.ToList());
        }

        // 
        // GET: /Assignment/Details/5

        public ActionResult Details(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }

            Device device = null;
            var allocatedDevice = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted == false).FirstOrDefault();
            if (allocatedDevice != null)
            {
                var deviceId = allocatedDevice.DeviceId;
                if (deviceId != 0)
                {
                    device = db.Devices.Find(deviceId);
                    ViewBag.Device = device.SerialNo;
                }
            }

            if (string.IsNullOrEmpty(ViewBag.Device))
            {
                ViewBag.Device = "Not allocated";
            }

            return View(vehicle);
        }

        //
        // GET: /Vehicle/Create

        public ActionResult Create()
        {
            var temp =db.Devices.Where(x=>!db.DeviceVehicles.Any(y=>y.DeviceId==x.DeviceId && y.Deleted!=true) && x.Deleted!=true).ToList();
            
            //ViewBag.RunningStatusId = new SelectList(db.RunningStatus_Master, "RunningStatusId", "RunningStatus");
            //ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName");
            ViewBag.BrandId = new SelectList(db.Brand_Master, "BrandId", "Brand");
            ViewBag.ModelId = new SelectList(db.VehicleModel_Master, "ModelId", "VehicleModel");
            ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo");
            return View();
        }

        //
        // POST: /Vehicle/Create

        [HttpPost]
        public ActionResult Create(Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                var device = Request["Device"];
                vehicle.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                vehicle.CreatedBy = userId;
                vehicle.Deleted = false;

                db.Vehicles.Add(vehicle);
                db.SaveChanges();

                if (!string.IsNullOrEmpty(device))
                {
                    int deviceId=Convert.ToInt32(device);
                    DeviceVehicle devicevehicle = new DeviceVehicle { DeviceId = deviceId, VehicleId = vehicle.VehicleId };
                    devicevehicle.CreatedDate = DateTime.Now;
                    devicevehicle.CreatedBy = userId;
                    devicevehicle.Deleted = false;
 
                    db.DeviceVehicles.Add(devicevehicle);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            //ViewBag.RunningStatusId = new SelectList(db.RunningStatus_Master, "RunningStatusId", "RunningStatus", vehicle.RunningStatusId);
            //ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", vehicle.DeptId);
            ViewBag.BrandId = new SelectList(db.Brand_Master, "BrandId", "Brand", vehicle.BrandId);
            ViewBag.ModelId = new SelectList(db.VehicleModel_Master, "ModelId", "VehicleModel",vehicle.ModelId);
            var temp = db.Devices.Where(x => !db.DeviceVehicles.Any(y => y.DeviceId == x.DeviceId && y.Deleted != true) && x.Deleted != true).ToList();
            ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo");
            return View(vehicle);
        }

        //
        // GET: /Vehicle/Edit/5

        public ActionResult Edit(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }

            Device device = null;
            var allocatedDevice = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted==false).FirstOrDefault();
            if (allocatedDevice != null)
            {
                var deviceId = allocatedDevice.DeviceId;
                if (deviceId!=0)
                {
                    device = db.Devices.Find(deviceId);
                }
            }

            //ViewBag.RunningStatusId = new SelectList(db.RunningStatus_Master, "RunningStatusId", "RunningStatus", vehicle.RunningStatusId);
            //ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", vehicle.DeptId);
            ViewBag.BrandId = new SelectList(db.Brand_Master, "BrandId", "Brand", vehicle.BrandId);
            ViewBag.ModelId = new SelectList(db.VehicleModel_Master, "ModelId", "VehicleModel", vehicle.ModelId);

            var temp = db.Devices.Where(x => !db.DeviceVehicles.Any(y => y.DeviceId == x.DeviceId && y.Deleted != true) && x.Deleted != true).ToList();

            if (device == null)
            {
                ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo");
            }
            else
            {
                temp.Add(device);
                ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo", device.DeviceId);
            }

            return View(vehicle);
        }

        //
        // POST: /Vehicle/Edit/5

        [HttpPost]
        public ActionResult Edit(Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                vehicle.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                vehicle.ModifiedBy = userId;

                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();

                var deviceIds = Request["Device"];
                if (!string.IsNullOrEmpty(deviceIds))
                {
                    int deviceId = Convert.ToInt32(deviceIds);

                    var result = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.DeviceId == deviceId && d.Deleted == false).FirstOrDefault();

                    if (result == null)
                    {
                        var result1 = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted == false).ToList();
                        foreach (var item in result1)
                        {
                            item.Deleted = true;
                        }

                        DeviceVehicle devicevehicle = new DeviceVehicle { DeviceId = deviceId, VehicleId = vehicle.VehicleId };
                        devicevehicle.CreatedDate = DateTime.Now;
                        devicevehicle.CreatedBy = userId;
                        devicevehicle.Deleted = false;

                        db.DeviceVehicles.Add(devicevehicle);
                        db.SaveChanges();
                    }
                }
                else
                {
                    var result1 = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted == false).ToList();
                    foreach (var item in result1)
                    {
                        item.Deleted = true;
                    }
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            //ViewBag.RunningStatusId = new SelectList(db.RunningStatus_Master, "RunningStatusId", "RunningStatus", vehicle.RunningStatusId);
            //ViewBag.DeptId = new SelectList(db.Departments, "DeptId", "DeptName", vehicle.DeptId);

            Device device = null;
            var allocatedDevice = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted == false).FirstOrDefault();
            if (allocatedDevice != null)
            {
                var deviceId = allocatedDevice.DeviceId;
                if (deviceId != 0)
                {
                    device = db.Devices.Find(deviceId);
                }
            }

            var temp = db.Devices.Where(x => !db.DeviceVehicles.Any(y => y.DeviceId == x.DeviceId && y.Deleted != true) && x.Deleted != true).ToList();

            if (device == null)
            {
                ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo");
            }
            else
            {
                temp.Add(device);
                ViewBag.DeviceId = new SelectList(temp, "DeviceId", "SerialNo", device.DeviceId);
            }

            ViewBag.BrandId = new SelectList(db.Brand_Master, "BrandId", "Brand", vehicle.BrandId);
            ViewBag.ModelId = new SelectList(db.VehicleModel_Master, "ModelId", "VehicleModel", vehicle.ModelId);
            return View(vehicle);
        }

        //
        // GET: /Vehicle/Delete/5

        public ActionResult Delete(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Vehicle vehicle = db.Vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        //
        // POST: /Vehicle/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Vehicle vehicle = db.Vehicles.Find(id);
            vehicle.Deleted = true;
            db.Entry(vehicle).State = EntityState.Modified;

            var result1 = db.DeviceVehicles.Where(d => d.VehicleId == vehicle.VehicleId && d.Deleted == false).ToList();
            foreach (var item in result1)
            {
                item.Deleted = true;
            }

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}