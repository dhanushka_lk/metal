﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using PagedList;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class RunningStatusController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /RunningStatus/

        public ActionResult Index(int? page)
        {
            var brands = db.RunningStatus_Master.Where(d => d.Deleted != true).OrderBy(b => b.RunningStatus);

            var result = WebConfigurationManager.AppSettings["RunningStatusPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(brands.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /RunningStatus/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /RunningStatus/Create

        [HttpPost]
        public ActionResult Create(RunningStatus_Master runningstatus_master)
        {
            if (ModelState.IsValid)
            {
                runningstatus_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                runningstatus_master.CreatedBy = userId;
                runningstatus_master.Deleted = false;

                db.RunningStatus_Master.Add(runningstatus_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(runningstatus_master);
        }

        //
        // GET: /RunningStatus/Edit/5

        public ActionResult Edit(int id = 0)
        {
            RunningStatus_Master runningstatus_master = db.RunningStatus_Master.Find(id);
            if (runningstatus_master == null)
            {
                return HttpNotFound();
            }
            return View(runningstatus_master);
        }

        //
        // POST: /RunningStatus/Edit/5

        [HttpPost]
        public ActionResult Edit(RunningStatus_Master runningstatus_master)
        {
            if (ModelState.IsValid)
            {
                runningstatus_master.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                runningstatus_master.ModifiedBy = userId;

                db.Entry(runningstatus_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(runningstatus_master);
        }

        //
        // GET: /RunningStatus/Delete/5

        public ActionResult Delete(int id = 0)
        {
            RunningStatus_Master runningstatus_master = db.RunningStatus_Master.Find(id);
            if (runningstatus_master == null)
            {
                return HttpNotFound();
            }
            return View(runningstatus_master);
        }

        //
        // POST: /RunningStatus/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            RunningStatus_Master runningstatus_master = db.RunningStatus_Master.Find(id);
            runningstatus_master.Deleted = true;
            db.Entry(runningstatus_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}