﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using PagedList;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class DeviceTypeController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /DeviceType/

        public ActionResult Index(int? page)
        {
            var devices = db.DeviceType_Master.Where(t=>t.Deleted!=true).OrderBy(d=>d.DeviceTypeId);

            var result = WebConfigurationManager.AppSettings["DeviceTypeListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(devices.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /DeviceType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DeviceType/Create

        [HttpPost]
        public ActionResult Create(DeviceType_Master devicetype_master)
        {
            if (ModelState.IsValid)
            {
                devicetype_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                devicetype_master.CreatedBy = userId;
                devicetype_master.Deleted = false;

                db.DeviceType_Master.Add(devicetype_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(devicetype_master);
        }

        //
        // GET: /DeviceType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DeviceType_Master devicetype_master = db.DeviceType_Master.Find(id);
            if (devicetype_master == null)
            {
                return HttpNotFound();
            }
            return View(devicetype_master);
        }

        //
        // POST: /DeviceType/Edit/5

        [HttpPost]
        public ActionResult Edit(DeviceType_Master devicetype_master)
        {
            if (ModelState.IsValid)
            {
                devicetype_master.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                devicetype_master.ModifiedBy = userId;
                db.Entry(devicetype_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(devicetype_master);
        }

        //
        // GET: /DeviceType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DeviceType_Master devicetype_master = db.DeviceType_Master.Find(id);
            if (devicetype_master == null)
            {
                return HttpNotFound();
            }
            return View(devicetype_master);
        }

        //
        // POST: /DeviceType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DeviceType_Master devicetype_master = db.DeviceType_Master.Find(id);
            devicetype_master.Deleted = true;
            db.Entry(devicetype_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}