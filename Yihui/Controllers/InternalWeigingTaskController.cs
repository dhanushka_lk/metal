﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize]
    public class InternalWeigingTaskController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /InternalWeigingTask/
        [Authorize(Roles = "BayWorker")]
        public ActionResult Index()
        {
            var userId = WebSecurity.GetUserId(User.Identity.Name);
            var assignments = db.WeigingTasks.OrderByDescending(a => a.ModifiedDate).Where(a => a.Deleted != true && a.UserId == userId);
            return View(assignments.ToList());
        }

        //
        // GET: /InternalWeigingTask/Details/5

        public ActionResult Details(int id = 0)
        {
            WeighingTask assignment = null;

            if (id == 0)
            {
                assignment = db.WeigingTasks.OrderByDescending(a => a.ModifiedDate).FirstOrDefault();
            }
            else
            {
                assignment = db.WeigingTasks.Find(id);
            }

            if (assignment == null)
            {
                return HttpNotFound();
            }

            return PartialView(assignment);

        }

        //
        // GET: /InternalWeigingTask/Create

        public ActionResult Create()
        {
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo");
            return View();
        }

        //
        // POST: /InternalWeigingTask/Create

        [HttpPost]
        public ActionResult Create(WeighingTask weighingtask)
        {
            if (ModelState.IsValid)
            {
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                weighingtask.CreatedBy = userId;
                weighingtask.CreatedDate = DateTime.Now;
                weighingtask.Deleted = false;
                weighingtask.JobId = "20150001";
                weighingtask.ModifiedBy = userId;
                weighingtask.ModifiedDate = DateTime.Now;
                weighingtask.StatusId = (int)Common.Status.NotSorted;
                weighingtask.TaskDate = DateTime.Now;
                weighingtask.TimeReceived = DateTime.Now;
                weighingtask.TimeWeighed = DateTime.Now;
                weighingtask.UserId = userId;

                db.WeigingTasks.Add(weighingtask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            return View(weighingtask);
        }

        //
        // GET: /InternalWeigingTask/Edit/5

        public ActionResult Edit(int id = 0)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            if (weighingtask == null)
            {
                return HttpNotFound();
            }
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", weighingtask.UserId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", weighingtask.StatusId);
            return View(weighingtask);
        }

        //
        // POST: /InternalWeigingTask/Edit/5

        [HttpPost]
        public ActionResult Edit(WeighingTask weighingtask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weighingtask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", weighingtask.UserId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", weighingtask.StatusId);
            return View(weighingtask);
        }

        //
        // GET: /InternalWeigingTask/Delete/5

        public ActionResult Delete(int id = 0)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            if (weighingtask == null)
            {
                return HttpNotFound();
            }
            return View(weighingtask);
        }

        //
        // POST: /InternalWeigingTask/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            db.WeigingTasks.Remove(weighingtask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}