﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using PagedList;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class ModelController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Model/

        public ActionResult Index(int? page)
        {
            var result = WebConfigurationManager.AppSettings["ModelListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(db.Model_Master.Where(m=>m.Deleted!=true).OrderBy(m=>m.ModelId).ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Model/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Model/Create

        [HttpPost]
        public ActionResult Create(Model_Master model_master)
        {
            if (ModelState.IsValid)
            {
                model_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                model_master.CreatedBy = userId;
                model_master.Deleted = false;

                db.Model_Master.Add(model_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model_master);
        }

        //
        // GET: /Model/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Model_Master model_master = db.Model_Master.Find(id);
            if (model_master == null)
            {
                return HttpNotFound();
            }
            return View(model_master);
        }

        //
        // POST: /Model/Edit/5

        [HttpPost]
        public ActionResult Edit(Model_Master model_master)
        {
            if (ModelState.IsValid)
            {
                model_master.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                model_master.ModifiedBy = userId;
                db.Entry(model_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model_master);
        }

        //
        // GET: /Model/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Model_Master model_master = db.Model_Master.Find(id);
            if (model_master == null)
            {
                return HttpNotFound();
            }
            return View(model_master);
        }

        //
        // POST: /Model/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Model_Master model_master = db.Model_Master.Find(id);
            model_master.Deleted = true;
            db.Entry(model_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}