﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using PagedList;
using WebMatrix.WebData;
using System.IO;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AssignmentController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Assignment/

        public ActionResult Index()
        {
            string page = string.Empty;
            var queryString = Request.QueryString.GetValues("grid-page");
            if (queryString != null && queryString.Count() > 0)
            {
                page = queryString.FirstOrDefault();
                ViewBag.Page = page;
            }

            var assignments = from t in db.Assignments
                           where t.Deleted != true
                           select t;

            return View(assignments.ToList());
        }

        //
        // GET: /Assignment/Details/5

        public ActionResult Details(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }

            ViewBag.items = db.AssignmentItems.Where(a => a.AssignmentId == id && a.Deleted == false).ToList();
            return View(assignment);
        }

        public ActionResult Reassign(int id = 0, int page = 0)
        {
            ViewBag.Page = page;
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.UserId = new SelectList(users, "UserId", "UserName");

            return View(assignment);
        }

        [HttpPost]
        public ActionResult Reassign()
        {
            if (ModelState.IsValid)
            {
                var newOwner = Request["UserId"];
                var id = Request["Id"];
                if (!string.IsNullOrEmpty(newOwner) && !string.IsNullOrEmpty(id))
                {
                    Assignment newAssignment = db.Assignments.Find(Convert.ToInt32(id));
                    if (newAssignment == null)
                    {
                        return HttpNotFound();
                    }

                    newAssignment.UserId =Convert.ToInt32(newOwner);

                    var userId = WebSecurity.GetUserId(User.Identity.Name);
                    newAssignment.ModifiedBy = userId;
                    newAssignment.ModifiedDate = DateTime.Now;
                    db.Entry(newAssignment).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.UserId = new SelectList(users, "UserId", "UserName");

            return RedirectToAction("Index");
        }

        public ActionResult Drivers(int id = 0)
        {
            string roleName = Common.UserRoles.Internal.ToString();

            var result = (from userRoles in db.UsersInRoles
                          join role in db.Roles on userRoles.RoleId equals role.RoleId
                          join user in db.Users on userRoles.UserId equals user.UserId
                          where role.RoleName == roleName && user.Deleted==false
                          select user).ToList();

            List<Driver> drivers = new List<Driver>();

            string EmptyVehicle = "not logged in";
            string EmptyJob = "not on job";

            foreach (var item in result)
            {
                Driver driver = new Driver { Name = item.FName + " " + item.LName, ContactNo = item.ContactNo };
                var userDevice = db.UserDevices.Where(d => d.UserId == item.UserId && d.LoggedIn == true).FirstOrDefault();
                if (userDevice != null)
                {
                    var deviceId = userDevice.DeviceId;
                    var deviceVehicle = db.DeviceVehicles.Where(dv => dv.DeviceId == deviceId && dv.Deleted == false).SingleOrDefault();
                    if (deviceVehicle != null)
                    {
                        var vehicle = db.Vehicles.Find(deviceVehicle.VehicleId);
                        if (vehicle != null)
                        {
                            driver.VehcileNo = vehicle.VehicleNo;
                            driver.Lat=userDevice.Lat;
                            driver.Lan=userDevice.Lan;
                        }
                    }

                }
                if (string.IsNullOrEmpty(driver.VehcileNo))
                {
                    driver.VehcileNo = EmptyVehicle;
                }

                int Progress=(int)Common.Status.InProgress;

                var assignment = db.Assignments.Where(a => a.UserId == item.UserId && a.StatusId == Progress).FirstOrDefault();

                if (assignment != null)
                {
                    driver.JobName = assignment.Name;
                    driver.CompanyName = assignment.Customer.CompName;
                }
                else
                {
                    driver.JobName = EmptyJob;
                    driver.CompanyName = EmptyJob;
                }

                drivers.Add(driver);

            }


            return View(drivers);
        }

        //
        // GET: /Assignment/Create

        public ActionResult Create()
        {
            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                          join role in db.Roles on userRoles.RoleId equals role.RoleId
                          join user in db.Users on userRoles.UserId equals user.UserId
                          where role.RoleName == userRole && user.Deleted != true
                          select user).ToList();

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority");
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status");
            ViewBag.UserId = new SelectList(users, "UserId", "UserName");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName");
            ViewBag.ItemId = new SelectList(db.Items.Where(a=>a.Deleted==false), "ItemId", "Name");
            return View();
        }

        //
        // POST: /Assignment/Create

        [HttpPost]
        public ActionResult Create(Assignment assignment, HttpPostedFileBase file)
        {
            var items = Request["ItemId"];
            var wheights = Request["Weight"];
            List<string> itemList=null;
            List<string> wheightList=null;

            if (!string.IsNullOrEmpty(items) && !string.IsNullOrEmpty(wheights))
            {
                itemList = items.Split(',').ToList();
                wheightList = wheights.Split(',').ToList();

                if (itemList.Count < 0 || wheightList.Count < 0)
                {
                    ModelState.AddModelError("Items", "Please select a item");
                }
            }
            else
            {
                ModelState.AddModelError("Items", "Please select a item");
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        string path = Server.MapPath("~/PO/");
                        string fullPath = System.IO.Path.Combine(path, pic);

                        if (!Directory.Exists(path))
                        {
                            DirectoryInfo di = Directory.CreateDirectory(path);
                        }

                        // file is uploaded
                        file.SaveAs(fullPath);
                        assignment.POPath = fullPath;
                }

                assignment.Deleted = false;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                assignment.CreatedBy = userId;
                assignment.CreatedDate = DateTime.Now;
                db.Assignments.Add(assignment);
                db.SaveChanges();

                for(int x=0;x<itemList.Count;x++)
                {
                    if (!string.IsNullOrEmpty(wheightList[x]))
                    {
                        AssignmentItem assignmentItem = new AssignmentItem { AssignmentId = assignment.AssignmentId, ItemId = Convert.ToInt32(itemList[x]), Weight = Convert.ToDouble(wheightList[x]) };
                        assignmentItem.CreatedBy = userId;
                        assignmentItem.CreatedDate = DateTime.Now;
                        assignment.ModifiedBy = userId;
                        assignment.ModifiedDate = DateTime.Now;
                        assignmentItem.Deleted = false;
                        db.AssignmentItems.Add(assignmentItem);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName");
            ViewBag.ItemId = new SelectList(db.Items.Where(a => a.Deleted == false), "ItemId", "Name");

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);
            return View(assignment);
        }

        //
        // GET: /Assignment/Edit/5

        public ActionResult Edit(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName",assignment.CustomerId);
            ViewBag.Items = db.Items.Where(a => a.Deleted == false).ToList();

            ViewBag.MyItems = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted==false).ToList();

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);

            return View(assignment);
        }

        //
        // POST: /Assignment/Edit/5

        [HttpPost]
        public ActionResult Edit(Assignment assignment, HttpPostedFileBase file)
        {
            var items = Request["ItemId"];
            var wheights = Request["Weight"];
            List<string> itemList = null;
            List<string> wheightList = null;

            if (!string.IsNullOrEmpty(items) && !string.IsNullOrEmpty(wheights))
            {
                itemList = items.Split(',').ToList();
                wheightList = wheights.Split(',').ToList();

                if (itemList.Count < 0 || wheightList.Count < 0)
                {
                    ModelState.AddModelError("Items", "Please select a item");
                }
            }
            else
            {
                ModelState.AddModelError("Items", "Please select a item");
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/PO/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    assignment.POPath = fullPath;
                }

                var userId = WebSecurity.GetUserId(User.Identity.Name);
                assignment.ModifiedBy = userId;
                assignment.ModifiedDate = DateTime.Now;
                db.Entry(assignment).State = EntityState.Modified;
                db.SaveChanges();

                var preValues = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted == false).ToList();
                foreach (var value in preValues)
                {
                    value.Deleted = true;
                }
                db.SaveChanges();

                for (int x = 0; x < itemList.Count; x++)
                {
                    if (!string.IsNullOrEmpty(wheightList[x]))
                    {
                        AssignmentItem assignmentItem = new AssignmentItem { AssignmentId = assignment.AssignmentId, ItemId = Convert.ToInt32(itemList[x]), Weight = Convert.ToDouble(wheightList[x]) };
                        assignmentItem.CreatedBy = userId;
                        assignmentItem.CreatedDate = DateTime.Now;
                        assignmentItem.Deleted = false;
                        db.AssignmentItems.Add(assignmentItem);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName",assignment.CustomerId);
            ViewBag.Items = db.Items.Where(a => a.Deleted == false).ToList();

            ViewBag.MyItems = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted == false).ToList();

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);

            return View(assignment);
        }

        //
        // GET: /Assignment/Delete/5

        public ActionResult Delete(int id = 0,int page=0)
        {
            ViewBag.Page = page;
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        //
        // POST: /Assignment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Assignment assignment = db.Assignments.Find(id);
            assignment.Deleted = true;
            db.Entry(assignment).State = EntityState.Modified;
            db.SaveChanges();

            var preValues = db.AssignmentItems.Where(a => a.AssignmentId == id && a.Deleted == false).ToList();
            foreach (var value in preValues)
            {
                value.Deleted = true;
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}