﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using PagedList;
using System.Web.Configuration;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class DepartmentController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Department/

        public ActionResult Index(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DeptNameSortParm = String.IsNullOrEmpty(sortOrder) ? "DeptNo_asc" : "";
            ViewBag.DeptNameSortParm = sortOrder == "DeptNo_asc" ? "DeptNo_desc" : "DeptNo_asc";
            ViewBag.OrgNameSortParm = sortOrder == "Org_asc" ? "Org_desc" : "Org_asc";

            var departments = db.Departments.Where(d => d.Deleted != true);

            switch (sortOrder)
            {
                case "DeptNo_asc":
                    departments = departments.OrderBy(s => s.DeptName);
                    break;
                case "DeptNo_desc":
                    departments = departments.OrderByDescending(s => s.DeptName);
                    break;
                case "Org_asc":
                    departments = departments.OrderBy(s => s.Organization.OrgName);
                    break;
                case "Org_desc":
                    departments = departments.OrderByDescending(s => s.Organization.OrgName);
                    break;
                default:
                    departments = departments.OrderBy(s => s.DeptName);
                    break;
            }

            var result = WebConfigurationManager.AppSettings["DepartmentListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(departments.ToPagedList(pageNumber, pageSize));
        }


        //
        // GET: /Department/Create

        public ActionResult Create()
        {
            ViewBag.OrgId = new SelectList(db.Organizations, "OrgId", "OrgName");
            ViewBag.DeptHeadUserId = new SelectList(db.Users, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Department/Create

        [HttpPost]
        public ActionResult Create(Department department)
        {
            var deptHead = Request["DeptHead"];
            if (!string.IsNullOrEmpty(deptHead))
            {
                department.DeptHead = Convert.ToInt32(deptHead);
            }

            if (ModelState.IsValid)
            {
                department.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                department.CreatedBy = userId;
                department.Deleted = false;

                db.Departments.Add(department);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrgId = new SelectList(db.Organizations, "OrgId", "OrgName", department.OrgId);
            ViewBag.DeptHeadUserId = new SelectList(db.Users, "UserId", "UserName", department.DeptId);
            return View(department);
        }

        //
        // GET: /Department/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrgId = new SelectList(db.Organizations, "OrgId", "OrgName", department.OrgId);
            ViewBag.DeptHeadUserId = new SelectList(db.Users, "UserId", "UserName", department.DeptHead);
            return View(department);
        }

        //
        // POST: /Department/Edit/5

        [HttpPost]
        public ActionResult Edit(Department department)
        {
            var deptHead = Request["DeptHead"];
            if (!string.IsNullOrEmpty(deptHead))
            {
                department.DeptHead = Convert.ToInt32(deptHead);
            }

            if (ModelState.IsValid)
            {
                department.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                department.ModifiedBy = userId;

                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrgId = new SelectList(db.Organizations, "OrgId", "OrgName", department.OrgId);
            ViewBag.DeptHeadUserId = new SelectList(db.Users, "UserId", "UserName", department.DeptHead);
            return View(department);
        }

        //
        // GET: /Department/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
        }

        //
        // POST: /Department/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Department department = db.Departments.Find(id);
            department.Deleted = true;
            db.Entry(department).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}