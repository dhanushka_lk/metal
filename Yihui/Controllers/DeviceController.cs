﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using System.Web.Configuration;
using PagedList;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class DeviceController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Device/

        public ActionResult Index(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.DeviceTypeSortParm = String.IsNullOrEmpty(sortOrder) ? "deviceType_asc" : "";
            ViewBag.DeviceTypeSortParm = sortOrder == "deviceType_asc" ? "deviceType_desc" : "deviceType_asc";
            ViewBag.SerialNoSortParm = sortOrder == "serialNo_asc" ? "serialNo_desc" : "serialNo_asc";
            ViewBag.ModelSortParm = sortOrder == "model_asc" ? "model_desc" : "model_asc";
            ViewBag.PurchaseDateSortParm = sortOrder == "date_asc" ? "date_desc" : "date_asc";

            var devices = db.Devices.Where(d=>d.Deleted!=true);

            switch (sortOrder)
            {
                case "deviceType_asc":
                    devices = devices.OrderBy(s => s.DeviceTypeId);
                    break;
                case "deviceType_desc":
                    devices = devices.OrderByDescending(s => s.DeviceTypeId);
                    break;
                case "serialNo_asc":
                    devices = devices.OrderBy(s => s.SerialNo);
                    break;
                case "serialNo_desc":
                    devices = devices.OrderByDescending(s => s.SerialNo);
                    break;
                case "model_asc":
                    devices = devices.OrderBy(s => s.ModelId);
                    break;
                case "model_desc":
                    devices = devices.OrderByDescending(s => s.ModelId);
                    break;
                case "date_asc":
                    devices = devices.OrderBy(s => s.PurchaseDate);
                    break;
                case "date_desc":
                    devices = devices.OrderByDescending(s => s.PurchaseDate);
                    break;
                default:
                    devices = devices.OrderByDescending(s => s.PurchaseDate);
                    break;
            }

            var result = WebConfigurationManager.AppSettings["DeviceListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(devices.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Device/Create

        public ActionResult Create()
        {
            ViewBag.ModelId = new SelectList(db.Model_Master, "ModelId", "Model");
            ViewBag.DeviceTypeId = new SelectList(db.DeviceType_Master, "DeviceTypeId", "DeviceType");
            return View();
        }

        //
        // POST: /Device/Create

        [HttpPost]
        public ActionResult Create(Device device)
        {
            if (ModelState.IsValid)
            {
                device.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                device.CreatedBy = userId;
                device.Deleted = false;

                db.Devices.Add(device);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ModelId = new SelectList(db.Model_Master, "ModelId", "Model", device.ModelId);
            ViewBag.DeviceTypeId = new SelectList(db.DeviceType_Master, "DeviceTypeId", "DeviceType", device.DeviceTypeId);
            return View(device);
        }

        //
        // GET: /Device/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModelId = new SelectList(db.Model_Master, "ModelId", "Model", device.ModelId);
            ViewBag.DeviceTypeId = new SelectList(db.DeviceType_Master, "DeviceTypeId", "DeviceType", device.DeviceTypeId);
            return View(device);
        }

        //
        // POST: /Device/Edit/5

        [HttpPost]
        public ActionResult Edit(Device device)
        {
            if (ModelState.IsValid)
            {
                device.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                device.ModifiedBy = userId;

                db.Entry(device).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ModelId = new SelectList(db.Model_Master, "ModelId", "Model", device.ModelId);
            ViewBag.DeviceTypeId = new SelectList(db.DeviceType_Master, "DeviceTypeId", "DeviceType", device.DeviceTypeId);
            return View(device);
        }

        //
        // GET: /Device/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        //
        // POST: /Device/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Device device = db.Devices.Find(id);
            device.Deleted = true;
            db.Entry(device).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}