﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using WebMatrix.WebData;
using System.IO;

namespace Yihui.Controllers
{
    [Authorize]
    public class InternalAssignmentController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /InternalAssignment/
        [Authorize(Roles = "Internal")]
        public ActionResult Index()
        {
            var userId = WebSecurity.GetUserId(User.Identity.Name);
            var assignments = db.Assignments.OrderByDescending(a => a.ModifiedDate).Where(a=>a.Deleted!=true && a.UserId==userId);
            return View(assignments.ToList());
        }

        //
        // GET: /InternalAssignment/Details/5
        [Authorize(Roles = "Internal")]
        public ActionResult Details(int id = 0)
        {
            Assignment assignment = null;

            if (id == 0)
            {
                assignment = db.Assignments.OrderByDescending(a => a.ModifiedDate).FirstOrDefault();
            }
            else
            {
                assignment = db.Assignments.Find(id);
            }

            if (assignment == null)
            {
                return HttpNotFound();
            }

            IEnumerable<Status_Master> states = null;

            if (assignment.StatusId == (int)Common.Status.Not_Started)
            {
                states = db.Status_Master;
            }
            else if (assignment.StatusId == (int)Common.Status.Completed)
            {
                int finish = (int)Common.Status.Completed;
                states = db.Status_Master.Where(s => s.StatusId == finish);
            }
            else 
            {
                int notStart = (int)Common.Status.Not_Started;
                states = db.Status_Master.Where(s => s.StatusId != notStart);
            }

            ViewBag.StatusId = new SelectList(states, "StatusId", "Status", assignment.StatusId);
            ViewBag.items = db.AssignmentItems.Where(a => a.AssignmentId == id && a.Deleted == false).ToList();
            return PartialView(assignment);
        }

        public FileResult DisplayPDF(int id)
        {
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return null;
            }

            if (!string.IsNullOrEmpty(assignment.POPath))
            {
                return File(assignment.POPath, "application/pdf");
            }

            return null;
        }

        //
        // GET: /InternalAssignment/ViewMap
        [Authorize(Roles = "Internal")]
        public ActionResult ViewMap(int id = 0)
        {
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }

            ViewBag.DesAddress = assignment.Address;

            return View();
        }

        //
        // GET: /InternalAssignment/Create
        [Authorize(Roles = "Internal")]
        public ActionResult Create()
        {
            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority");
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status");
            ViewBag.UserId = new SelectList(users, "UserId", "UserName");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName");
            ViewBag.ItemId = new SelectList(db.Items.Where(a => a.Deleted == false), "ItemId", "Name");
            return PartialView();
        }

        //
        // POST: /InternalAssignment/Create
        [Authorize(Roles = "Internal")]
        [HttpPost]
        public ActionResult Create(Assignment assignment, HttpPostedFileBase file)
        {
            var items = Request["ItemId"];
            var wheights = Request["Weight"];
            List<string> itemList = null;
            List<string> wheightList = null;

            if (!string.IsNullOrEmpty(items) && !string.IsNullOrEmpty(wheights))
            {
                itemList = items.Split(',').ToList();
                wheightList = wheights.Split(',').ToList();

                if (itemList.Count < 0 || wheightList.Count < 0)
                {
                    ModelState.AddModelError("Items", "Please select a item");
                }
            }
            else
            {
                ModelState.AddModelError("Items", "Please select a item");
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/PO/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    assignment.POPath = fullPath;
                }

                var userId = WebSecurity.GetUserId(User.Identity.Name);
                assignment.UserId = userId;
                assignment.Deleted = false;
                assignment.CreatedDate = DateTime.Now;
                assignment.CreatedBy = userId;
                assignment.ModifiedBy = userId;
                assignment.ModifiedDate = DateTime.Now;
                db.Assignments.Add(assignment);
                db.SaveChanges();

                for (int x = 0; x < itemList.Count; x++)
                {
                    if (!string.IsNullOrEmpty(wheightList[x]))
                    {
                        AssignmentItem assignmentItem = new AssignmentItem { AssignmentId = assignment.AssignmentId, ItemId = Convert.ToInt32(itemList[x]), Weight = Convert.ToDouble(wheightList[x]) };
                        assignmentItem.CreatedBy = userId;
                        assignmentItem.CreatedDate = DateTime.Now;
                        assignmentItem.Deleted = false;
                        db.AssignmentItems.Add(assignmentItem);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName");
            ViewBag.ItemId = new SelectList(db.Items.Where(a => a.Deleted == false), "ItemId", "Name");
            return View(assignment);
        }

        //
        // GET: /InternalAssignment/Edit/5
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(int id = 0)
        {
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName", assignment.CustomerId);

            ViewBag.Items = db.Items.Where(a => a.Deleted == false).ToList();
            ViewBag.MyItems = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted == false).ToList();

            return View(assignment);
        }

        //
        // POST: /InternalAssignment/Edit/5

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(Assignment assignment, HttpPostedFileBase file)
        {
            var items = Request["ItemId"];
            var wheights = Request["Weight"];
            List<string> itemList = null;
            List<string> wheightList = null;

            if (!string.IsNullOrEmpty(items) && !string.IsNullOrEmpty(wheights))
            {
                itemList = items.Split(',').ToList();
                wheightList = wheights.Split(',').ToList();

                if (itemList.Count < 0 || wheightList.Count < 0)
                {
                    ModelState.AddModelError("Items", "Please select a item");
                }
            }
            else
            {
                ModelState.AddModelError("Items", "Please select a item");
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/PO/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    assignment.POPath = fullPath;
                }

                assignment.Deleted = false;
                assignment.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                assignment.ModifiedBy = userId;

                db.Entry(assignment).State = EntityState.Modified;
                db.SaveChanges();

                var preValues = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted == false).ToList();
                foreach (var value in preValues)
                {
                    value.Deleted = true;
                }
                db.SaveChanges();

                for (int x = 0; x < itemList.Count; x++)
                {
                    if (!string.IsNullOrEmpty(wheightList[x]))
                    {
                        AssignmentItem assignmentItem = new AssignmentItem { AssignmentId = assignment.AssignmentId, ItemId = Convert.ToInt32(itemList[x]), Weight = Convert.ToDouble(wheightList[x]) };
                        assignmentItem.CreatedBy = userId;
                        assignmentItem.CreatedDate = DateTime.Now;
                        assignmentItem.Deleted = false;
                        db.AssignmentItems.Add(assignmentItem);
                        db.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }

            string userRole = Common.UserRoles.Internal.ToString();

            var users = (from userRoles in db.UsersInRoles
                         join role in db.Roles on userRoles.RoleId equals role.RoleId
                         join user in db.Users on userRoles.UserId equals user.UserId
                         where role.RoleName == userRole && user.Deleted != true
                         select user).ToList();

            ViewBag.PriorityId = new SelectList(db.Priority_Master, "PriorityId", "Priority", assignment.PriorityId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", assignment.StatusId);
            ViewBag.UserId = new SelectList(users, "UserId", "UserName", assignment.UserId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CompName",assignment.CustomerId);
            ViewBag.Items = db.Items.Where(a => a.Deleted == false).ToList();
            ViewBag.MyItems = db.AssignmentItems.Where(a => a.AssignmentId == assignment.AssignmentId && a.Deleted == false).ToList();
            return View(assignment);
        }

        //
        // GET: /InternalAssignment/Delete/5
        [Authorize(Roles = "Internal")]
        public ActionResult Delete(int id = 0)
        {
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        //
        // POST: /InternalAssignment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Assignment assignment = db.Assignments.Find(id);
            assignment.Deleted = true;
            db.Entry(assignment).State = EntityState.Modified;
            db.SaveChanges();

            var preValues = db.AssignmentItems.Where(a => a.AssignmentId == id && a.Deleted == false).ToList();
            foreach (var value in preValues)
            {
                value.Deleted = true;
            }
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetCustomerDetails(int cusId)
        {
            Customer customer = db.Customers.Find(cusId);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return Json(customer, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateStatus(int id,int statusid)
        {
            Assignment assignment = db.Assignments.Find(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }

            assignment.StatusId = statusid;
            assignment.ModifiedDate = DateTime.Now;
            var userId = WebSecurity.GetUserId(User.Identity.Name);
            assignment.ModifiedBy = userId;

            db.Entry(assignment).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}