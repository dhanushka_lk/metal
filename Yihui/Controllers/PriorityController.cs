﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class PriorityController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Priority/

        public ActionResult Index()
        {
            return View(db.Priority_Master.Where(p=>p.Deleted!=true).ToList());
        }

        //
        // GET: /Priority/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Priority/Create

        [HttpPost]
        public ActionResult Create(Priority_Master priority_master)
        {
            if (ModelState.IsValid)
            {
                priority_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                priority_master.CreatedBy = userId;
                priority_master.Deleted = false;
                db.Priority_Master.Add(priority_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(priority_master);
        }

        //
        // GET: /Priority/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Priority_Master priority_master = db.Priority_Master.Find(id);
            if (priority_master == null)
            {
                return HttpNotFound();
            }
            return View(priority_master);
        }

        //
        // POST: /Priority/Edit/5

        [HttpPost]
        public ActionResult Edit(Priority_Master priority_master)
        {
            if (ModelState.IsValid)
            {
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                priority_master.ModifiedBy = userId;
                priority_master.ModifiedDate = DateTime.Now;
                db.Entry(priority_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(priority_master);
        }

        //
        // GET: /Priority/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Priority_Master priority_master = db.Priority_Master.Find(id);
            if (priority_master == null)
            {
                return HttpNotFound();
            }
            return View(priority_master);
        }

        //
        // POST: /Priority/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Priority_Master priority_master = db.Priority_Master.Find(id);
            priority_master.Deleted = true;
            db.Entry(priority_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}