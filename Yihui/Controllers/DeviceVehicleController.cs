﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Entity;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Yihui.Models;
//using Yihui.DAL;
//using System.Web.Configuration;
//using PagedList;
//using WebMatrix.WebData;

//namespace Yihui.Controllers
//{
//    public class DeviceVehicleController : Controller
//    {
//        private YihuiEntities db = new YihuiEntities();

//        //
//        // GET: /DeviceVehicle/

//        public ActionResult Index(string sortOrder, string currentSerial, string searchSerial, string currentVehicle, string searchVehicle, int? page)
//        {
//            ViewBag.CurrentSort = sortOrder;
//            ViewBag.SerialNoSortParm = String.IsNullOrEmpty(sortOrder) ? "serial_asc" : "";
//            ViewBag.SerialNoSortParm = sortOrder == "serial_asc" ? "serial_desc" : "serial_asc";
//            ViewBag.VehicleNoSortParm = sortOrder == "vehicle_asc" ? "vehicle_desc" : "vehicle_asc";

//            if (searchVehicle != null || searchSerial != null)
//            {
//                page = 1;
//            }
//            if (string.IsNullOrEmpty(searchSerial))
//            {
//                searchSerial = currentSerial;
//            }
//            if (string.IsNullOrEmpty(searchVehicle))
//            {
//                searchVehicle = currentVehicle;
//            }

//            ViewBag.CurrentSerial = searchSerial;
//            ViewBag.CurrentVehicle = searchVehicle;

//            var devicevehicles = from t in db.DeviceVehicles
//                                 where t.Active == true && t.Deleted!=true
//                                 select t;

//            if (!String.IsNullOrEmpty(searchSerial))
//            {
//                devicevehicles = devicevehicles.Where(d => d.Device.SerialNo.ToLower().Contains(searchSerial.ToLower()));
//            }

//            if (!String.IsNullOrEmpty(searchVehicle))
//            {
//                devicevehicles = devicevehicles.Where(d => d.Vehicle.VehicleNo.ToLower().Contains(searchVehicle.ToLower()));
//            }

//            switch (sortOrder)
//            {
//                case "serial_asc":
//                    devicevehicles = devicevehicles.OrderBy(s => s.Device.SerialNo);
//                    break;
//                case "serial_desc":
//                    devicevehicles = devicevehicles.OrderByDescending(s => s.Device.SerialNo);
//                    break;
//                case "vehicle_asc":
//                    devicevehicles = devicevehicles.OrderBy(s => s.Vehicle.VehicleNo);
//                    break;
//                case "vehicle_desc":
//                    devicevehicles = devicevehicles.OrderByDescending(s => s.Vehicle.VehicleNo);
//                    break;
//                default:
//                    devicevehicles = devicevehicles.OrderBy(s => s.Device.SerialNo);
//                    break;
//            }

//            var result = WebConfigurationManager.AppSettings["DeviceVehiclePageSize"];
//            int pageSize = 5;
//            if (!string.IsNullOrEmpty(result))
//            {
//                pageSize = Convert.ToInt32(result);
//            }

//            int pageNumber = (page ?? 1);
//            return View(devicevehicles.ToPagedList(pageNumber, pageSize));
//        }

//        //
//        // GET: /DeviceVehicle/Create

//        public ActionResult Create()
//        {
//            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo");
//            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "SerialNo");
//            return View();
//        }

//        //
//        // POST: /DeviceVehicle/Create

//        [HttpPost]
//        public ActionResult Create(DeviceVehicle devicevehicle)
//        {
//            if (ModelState.IsValid)
//            {
//                devicevehicle.CreatedDate = DateTime.Now;
//                var userId = WebSecurity.GetUserId(User.Identity.Name);
//                devicevehicle.CreatedBy = userId;
//                devicevehicle.Deleted = false;
//                devicevehicle.Active = true;

//                var preValues = db.DeviceVehicles.Where(d=>d.DeviceId == devicevehicle.DeviceId).ToList();

//                if (preValues != null)
//                {
//                    foreach (var value in preValues)
//                    {
//                        value.Active = false;
//                    }
//                }

//                db.DeviceVehicles.Add(devicevehicle);
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }

//            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", devicevehicle.VehicleId);
//            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "SerialNo", devicevehicle.DeviceId);
//            return View(devicevehicle);
//        }

//        //
//        // GET: /DeviceVehicle/Edit/5

//        public ActionResult Edit(int id = 0)
//        {
//            DeviceVehicle devicevehicle = db.DeviceVehicles.Find(id);
//            if (devicevehicle == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", devicevehicle.VehicleId);
//            ViewBag.Device = devicevehicle.Device.SerialNo;
//            return View(devicevehicle);
//        }

//        //
//        // POST: /DeviceVehicle/Edit/5

//        [HttpPost]
//        public ActionResult Edit(DeviceVehicle devicevehicle)
//        {
//            if (ModelState.IsValid)
//            {
//                var preValues = db.DeviceVehicles.Where(d => d.DeviceId == devicevehicle.DeviceId).ToList();

//                if (preValues != null)
//                {
//                    foreach (var value in preValues)
//                    {
//                        value.Active = false;
//                    }
//                }

//                devicevehicle.ModifiedDate = DateTime.Now;
//                var userId = WebSecurity.GetUserId(User.Identity.Name);
//                devicevehicle.ModifiedBy = userId;

//                db.Entry(devicevehicle).State = EntityState.Modified;
//                db.SaveChanges();
//                return RedirectToAction("Index");
//            }
//            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", devicevehicle.VehicleId);
//            ViewBag.Device = devicevehicle.Device.SerialNo;
//            return View(devicevehicle);
//        }

//        //
//        // GET: /DeviceVehicle/Delete/5

//        public ActionResult Delete(int id = 0)
//        {
//            DeviceVehicle devicevehicle = db.DeviceVehicles.Find(id);
//            if (devicevehicle == null)
//            {
//                return HttpNotFound();
//            }
//            return View(devicevehicle);
//        }

//        //
//        // POST: /DeviceVehicle/Delete/5

//        [HttpPost, ActionName("Delete")]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            DeviceVehicle devicevehicle = db.DeviceVehicles.Find(id);
//            devicevehicle.Deleted = true;
//            db.Entry(devicevehicle).State = EntityState.Modified;
//            db.SaveChanges();
//            return RedirectToAction("Index");
//        }

//        protected override void Dispose(bool disposing)
//        {
//            db.Dispose();
//            base.Dispose(disposing);
//        }
//    }
//}