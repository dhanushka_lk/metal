﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;

namespace Yihui.Controllers
{
    public class WeighingTaskController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /WeighingTask/

        public ActionResult Index()
        {
            var weigingtask = db.WeigingTasks.Include(w => w.Vehicle).Include(w => w.Customer).Include(w => w.User).Include(w => w.Status_Master);
            return View(weigingtask.ToList());
        }

        //
        // GET: /WeighingTask/Details/5

        public ActionResult Details(int id = 0)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            if (weighingtask == null)
            {
                return HttpNotFound();
            }
            return View(weighingtask);
        }

        //
        // GET: /WeighingTask/Create

        public ActionResult Create()
        {
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo");
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo");
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName");
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status");
            return View();
        }

        //
        // POST: /WeighingTask/Create

        [HttpPost]
        public ActionResult Create(WeighingTask weighingtask)
        {
            if (ModelState.IsValid)
            {
                db.WeigingTasks.Add(weighingtask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", weighingtask.UserId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", weighingtask.StatusId);
            return View(weighingtask);
        }

        //
        // GET: /WeighingTask/Edit/5

        public ActionResult Edit(int id = 0)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            if (weighingtask == null)
            {
                return HttpNotFound();
            }
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", weighingtask.UserId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", weighingtask.StatusId);
            return View(weighingtask);
        }

        //
        // POST: /WeighingTask/Edit/5

        [HttpPost]
        public ActionResult Edit(WeighingTask weighingtask)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weighingtask).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VehicleId = new SelectList(db.Vehicles, "VehicleId", "VehicleNo", weighingtask.VehicleId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "CustomerNo", weighingtask.CustomerId);
            ViewBag.UserId = new SelectList(db.Users, "UserId", "UserName", weighingtask.UserId);
            ViewBag.StatusId = new SelectList(db.Status_Master, "StatusId", "Status", weighingtask.StatusId);
            return View(weighingtask);
        }

        //
        // GET: /WeighingTask/Delete/5

        public ActionResult Delete(int id = 0)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            if (weighingtask == null)
            {
                return HttpNotFound();
            }
            return View(weighingtask);
        }

        //
        // POST: /WeighingTask/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            WeighingTask weighingtask = db.WeigingTasks.Find(id);
            db.WeigingTasks.Remove(weighingtask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}