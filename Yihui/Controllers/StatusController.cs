﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class StatusController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Status/

        public ActionResult Index()
        {
            return View(db.Status_Master.Where(s=>s.Deleted!=true).ToList());
        }

        //
        // GET: /Status/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Status/Create

        [HttpPost]
        public ActionResult Create(Status_Master status_master)
        {
            if (ModelState.IsValid)
            {
                status_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                status_master.CreatedBy = userId;
                status_master.Deleted = false;
                db.Status_Master.Add(status_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(status_master);
        }

        //
        // GET: /Status/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Status_Master status_master = db.Status_Master.Find(id);
            if (status_master == null)
            {
                return HttpNotFound();
            }
            return View(status_master);
        }

        //
        // POST: /Status/Edit/5

        [HttpPost]
        public ActionResult Edit(Status_Master status_master)
        {
            if (ModelState.IsValid)
            {
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                status_master.ModifiedBy = userId;
                status_master.ModifiedDate = DateTime.Now;
                db.Entry(status_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(status_master);
        }

        //
        // GET: /Status/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Status_Master status_master = db.Status_Master.Find(id);
            if (status_master == null)
            {
                return HttpNotFound();
            }
            return View(status_master);
        }

        //
        // POST: /Status/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Status_Master status_master = db.Status_Master.Find(id);
            status_master.Deleted = true;
            db.Entry(status_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}