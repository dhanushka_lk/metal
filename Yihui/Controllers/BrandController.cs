﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;
using PagedList;
using System.Web.Configuration;
using WebMatrix.WebData;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class BrandController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /Brand/

        public ActionResult Index(int? page)
        {
            var brands = db.Brand_Master.Where(d => d.Deleted != true).OrderBy(b=>b.Brand);

            var result = WebConfigurationManager.AppSettings["BrandListPageSize"];
            int pageSize = 5;
            if (!string.IsNullOrEmpty(result))
            {
                pageSize = Convert.ToInt32(result);
            }

            int pageNumber = (page ?? 1);
            return View(brands.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Brand/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Brand/Create

        [HttpPost]
        public ActionResult Create(Brand_Master brand_master)
        {
            if (ModelState.IsValid)
            {
                brand_master.CreatedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                brand_master.CreatedBy = userId;
                brand_master.Deleted = false;

                db.Brand_Master.Add(brand_master);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(brand_master);
        }

        //
        // GET: /Brand/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Brand_Master brand_master = db.Brand_Master.Find(id);
            if (brand_master == null)
            {
                return HttpNotFound();
            }
            return View(brand_master);
        }

        //
        // POST: /Brand/Edit/5

        [HttpPost]
        public ActionResult Edit(Brand_Master brand_master)
        {
            if (ModelState.IsValid)
            {
                brand_master.ModifiedDate = DateTime.Now;
                var userId = WebSecurity.GetUserId(User.Identity.Name);
                brand_master.ModifiedBy = userId;

                db.Entry(brand_master).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(brand_master);
        }

        //
        // GET: /Brand/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Brand_Master brand_master = db.Brand_Master.Find(id);
            if (brand_master == null)
            {
                return HttpNotFound();
            }
            return View(brand_master);
        }

        //
        // POST: /Brand/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Brand_Master brand_master = db.Brand_Master.Find(id);
            brand_master.Deleted = true;
            db.Entry(brand_master).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}