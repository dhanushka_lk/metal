﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;

namespace Yihui.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class CustomerTypeController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /CustomerType/

        public ActionResult Index()
        {
            return View(db.CustomerTypes.ToList());
        }

        //
        // GET: /CustomerType/Details/5

        public ActionResult Details(int id = 0)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        //
        // GET: /CustomerType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /CustomerType/Create

        [HttpPost]
        public ActionResult Create(CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.CustomerTypes.Add(customertype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(customertype);
        }

        //
        // GET: /CustomerType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        //
        // POST: /CustomerType/Edit/5

        [HttpPost]
        public ActionResult Edit(CustomerType customertype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customertype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customertype);
        }

        //
        // GET: /CustomerType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            if (customertype == null)
            {
                return HttpNotFound();
            }
            return View(customertype);
        }

        //
        // POST: /CustomerType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CustomerType customertype = db.CustomerTypes.Find(id);
            db.CustomerTypes.Remove(customertype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}