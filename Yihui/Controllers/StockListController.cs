﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yihui.Models;
using Yihui.DAL;

namespace Yihui.Controllers
{
    public class StockListController : Controller
    {
        private YihuiEntities db = new YihuiEntities();

        //
        // GET: /StockList/

        public ActionResult Index()
        {
            var stocklists = db.StockLists.Where(s=>s.Deleted==false);
            return View(stocklists.ToList());
        }

        //
        // GET: /StockList/Details/5

        public ActionResult Details(int id = 0)
        {
            StockList stocklist = db.StockLists.Find(id);
            if (stocklist == null)
            {
                return HttpNotFound();
            }
            return View(stocklist);
        }

        //
        // GET: /StockList/Create

        public ActionResult Create()
        {
            ViewBag.ItemId = new SelectList(db.Items, "ItemId", "Name");
            ViewBag.WeighingTaskId = new SelectList(db.WeigingTasks, "WeighingTaskId", "JobId");
            return View();
        }

        //
        // POST: /StockList/Create

        [HttpPost]
        public ActionResult Create(StockList stocklist)
        {
            if (ModelState.IsValid)
            {
                db.StockLists.Add(stocklist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ItemId = new SelectList(db.Items, "ItemId", "Name", stocklist.ItemId);
            ViewBag.WeighingTaskId = new SelectList(db.WeigingTasks, "WeighingTaskId", "JobId", stocklist.WeighingTaskId);
            return View(stocklist);
        }

        //
        // GET: /StockList/Edit/5

        public ActionResult Edit(int id = 0)
        {
            StockList stocklist = db.StockLists.Find(id);
            if (stocklist == null)
            {
                return HttpNotFound();
            }
            ViewBag.ItemId = new SelectList(db.Items, "ItemId", "Name", stocklist.ItemId);
            ViewBag.WeighingTaskId = new SelectList(db.WeigingTasks, "WeighingTaskId", "JobId", stocklist.WeighingTaskId);
            return View(stocklist);
        }

        //
        // POST: /StockList/Edit/5

        [HttpPost]
        public ActionResult Edit(StockList stocklist)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stocklist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ItemId = new SelectList(db.Items, "ItemId", "Name", stocklist.ItemId);
            ViewBag.WeighingTaskId = new SelectList(db.WeigingTasks, "WeighingTaskId", "JobId", stocklist.WeighingTaskId);
            return View(stocklist);
        }

        //
        // GET: /StockList/Delete/5

        public ActionResult Delete(int id = 0)
        {
            StockList stocklist = db.StockLists.Find(id);
            if (stocklist == null)
            {
                return HttpNotFound();
            }
            return View(stocklist);
        }

        //
        // POST: /StockList/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            StockList stocklist = db.StockLists.Find(id);
            db.StockLists.Remove(stocklist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}