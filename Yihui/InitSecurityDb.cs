﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using Yihui.Common;
using Yihui.DAL;

namespace Yihui
{
    public class InitSecurityDb : DropCreateDatabaseIfModelChanges<YihuiEntities>
    {
        protected override void Seed(YihuiEntities context)
        {
            WebSecurity.InitializeDatabaseConnection("YihuiConnection", "User", "UserId", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(UserRoles.SuperAdmin.ToString()))
            {
                roles.CreateRole(UserRoles.SuperAdmin.ToString());
            }

            if (!roles.RoleExists(UserRoles.Admin.ToString()))
            {
                roles.CreateRole(UserRoles.Admin.ToString());
            }

            if (!roles.RoleExists(UserRoles.Internal.ToString()))
            {
                roles.CreateRole(UserRoles.Internal.ToString());
            }

            if (!roles.RoleExists(UserRoles.BayWorker.ToString()))
            {
                roles.CreateRole(UserRoles.BayWorker.ToString());
            }

            IDictionary<string, object> values = new Dictionary<string, object>();

            values.Add("DateOfJoin", DateTime.Now);
            values.Add("IsActive", true);

            if (membership.GetUser("SuperAdmin", false) == null)
            {
                membership.CreateUserAndAccount("SuperAdmin", "123456",false,values);
            }

            if (!roles.GetRolesForUser("SuperAdmin").Contains("SuperAdmin"))
            {
                roles.AddUsersToRoles(new[] { "SuperAdmin" }, new[] { "SuperAdmin" });
            }

            var baseDir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", string.Empty);

            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDir + "\\InitialData.sql"));

            context.SaveChanges();
        }
    }
}