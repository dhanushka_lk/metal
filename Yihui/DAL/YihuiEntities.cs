﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Yihui.Models;

namespace Yihui.DAL
{
    public class YihuiEntities:DbContext
    {
        public YihuiEntities()
            : base("YihuiConnection")
        {
        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<WeighingTask> WeigingTasks { get; set; }
        public DbSet<AssignmentTransfer> AssignmentTransfers { get; set; }
        public DbSet<Priority_Master> Priority_Master { get; set; }
        public DbSet<Status_Master> Status_Master { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Brand_Master> Brand_Master { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceType_Master> DeviceType_Master { get; set; }
        public DbSet<Model_Master> Model_Master { get; set; }
        public DbSet<VehicleModel_Master> VehicleModel_Master { get; set; }
        public DbSet<RunningStatus_Master> RunningStatus_Master { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<DeviceVehicle> DeviceVehicles { get; set; }
        public DbSet<UserDevice> UserDevices { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<AssignmentItem> AssignmentItems { get; set; }
        public DbSet<StockList> StockLists { get; set; }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UsersInRole> UsersInRoles { get; set; }

        public DbSet<CustomerType> CustomerTypes { get; set; }
    }
}