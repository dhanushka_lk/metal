﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Assignment")]
    public class Assignment : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int AssignmentId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Priority")]
        public int PriorityId { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [Required]
        [Display(Name = "Planned Start")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "Planned End")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Assignment Owner")]
        public int UserId { get; set; }

        [Display(Name = "Company Name")]
        public int CustomerId { get; set; }

        [Display(Name = "POPath")]
        public string POPath { get; set; }

        [Display(Name = "Delivery Address")]
        public string Address { get; set; }

        public virtual Priority_Master Priority_Master { get; set; }

        public virtual Status_Master Status_Master { get; set; }

        public virtual User User { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}