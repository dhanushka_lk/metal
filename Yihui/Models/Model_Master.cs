﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Model_Master")]
    public class Model_Master : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ModelId { get; set; }

        [Display(Name = "Model")]
        public string Model { get; set; }
    }
}