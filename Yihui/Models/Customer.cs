﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Customer")]
    public class Customer : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CustomerId { get; set; }

        [Display(Name = "Customer No")]
        public string CustomerNo { get; set; }

        [Display(Name = "Company Name")]
        public string CompName { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone No")]
        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }

        [Display(Name = "Contact Person")]
        public string ContactPerson { get; set; }

        [Display(Name = "GST Registered")]
        public bool GSTRegistered { get; set; }

        [Display(Name = "Customer Type")]
        public int CustomerTypeId { get; set; }

        public virtual CustomerType CustomerType { get; set; }
    }
}