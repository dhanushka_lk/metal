﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("DeviceType_Master")]
    public class DeviceType_Master : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DeviceTypeId { get; set; }

        [Display(Name = "Status")]
        public string DeviceType { get; set; }
    }
}