﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("RunningStatus_Master")]
    public class RunningStatus_Master : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RunningStatusId { get; set; }

        [Display(Name = "Status")]
        public string RunningStatus { get; set; }
    }
}