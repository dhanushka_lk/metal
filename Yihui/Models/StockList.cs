﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("StockList")]
    public class StockList : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int StockListId { get; set; }

        public int WeighingTaskId { get; set; }

        public string Tag { get; set; }

        public string Location { get; set; }

        public decimal Total { get; set; }

        public int ItemId { get; set; }

        public virtual Item Item { get; set; }

        public virtual WeighingTask WeighingTask { get; set; }
    }
}