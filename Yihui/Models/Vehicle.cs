﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Vehicle")]
    public class Vehicle : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int VehicleId { get; set; }

        [Display(Name = "Vehicle No")]
        public string VehicleNo { get; set; }

        [Display(Name = "COE Expiry")]
        public string COEExpiry { get; set; }

        //[Display(Name = "Current Running Status")]
        //public int RunningStatusId { get; set; }

        //[Display(Name = "Assigned Department")]
        //public int DeptId { get; set; }

        [Display(Name = "Brand")]
        public int BrandId { get; set; }

        [Display(Name = "Model")]
        public int ModelId { get; set; }

        //public virtual RunningStatus_Master CurrentRunningStatus { get; set; }

        //public virtual Department AssignedDept { get; set; }

        public virtual Brand_Master Brand { get; set; }

        public virtual VehicleModel_Master Model { get; set; }
    }
}