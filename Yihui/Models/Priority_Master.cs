﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Priority_Master")]
    public class Priority_Master : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PriorityId { get; set; }

        [Display(Name = "Priority")]
        public string Priority { get; set; }
    }
}