﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("WeighingTask")]
    public class WeighingTask : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int WeighingTaskId { get; set; }

        [Display(Name = "Job Id")]
        public string JobId { get; set; }

        [Display(Name = "Vehicle")]
        public int VehicleId { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Task Date")]
        public DateTime TaskDate { get; set; }

        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        [Display(Name = "Time Received")]
        public DateTime TimeReceived { get; set; }

        [DataType(DataType.Time)]
        [Display(Name = "Time Weighed")]
        public DateTime TimeWeighed { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }

        [Display(Name = "Total Weight")]
        public decimal TotalWeight { get; set; }

        [Display(Name = "Weight of Bin")]
        public decimal BinWeight { get; set; }

        [Display(Name = "Weight of metal")]
        public decimal MetalWeight { get; set; }

        [Display(Name = "Status")]
        public int StatusId { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual User User { get; set; }

        public virtual Status_Master Status_Master { get; set; }


    }
}