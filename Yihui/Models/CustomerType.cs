﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("CustomerType")]
    public class CustomerType : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CustomerTypeId { get; set; }

        [Display(Name = "CustomerType")]
        public string CustomerTypeName { get; set; }
    }
}