﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Item")]
    public class Item : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ItemId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Gauge")]
        public int Gauge { get; set; }

        [Display(Name = "Grade")]
        public int Grade { get; set; }

        [Display(Name = "UnitPrice")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}