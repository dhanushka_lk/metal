﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Organization")]
    public class Organization:BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int OrgId { get; set; }

        [Display(Name = "Organization Name")]
        public string OrgName { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Contact No")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Invalid Phone No")]
        public string ContactNo { get; set; }

        [Display(Name = "Website")]
        public string Website { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}