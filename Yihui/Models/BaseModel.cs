﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    //Base Model For Entities
    public abstract class BaseModel
    {
        public Nullable<DateTime> CreatedDate { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> Deleted { get; set; }
    }
}