﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("Department")]
    public class Department : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DeptId { get; set; }

        [Display(Name = "Department Name")]
        public string DeptName { get; set; }

        [Display(Name = "Organization")]
        public int OrgId { get; set; }

        [Display(Name = "Department Head")]
        public int DeptHead { get; set; }

        public virtual Organization Organization { get; set; }

    }
}