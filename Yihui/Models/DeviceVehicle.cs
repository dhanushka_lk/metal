﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("DeviceVehicle")]
    public class DeviceVehicle : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DeviceVehicleId { get; set; }

        [Display(Name = "Device")]
        public int DeviceId { get; set; }

        [Display(Name = "Vehicle")]
        public int VehicleId { get; set; }

        public virtual Vehicle Vehicle { get; set; }
        public virtual Device Device { get; set; }
    }
}