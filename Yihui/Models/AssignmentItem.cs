﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("AssignmentItem")]
    public class AssignmentItem : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int AssignmentItemId { get; set; }

        [Display(Name = "Assignment")]
        public int AssignmentId { get; set; }

        [Display(Name = "Item")]
        public int ItemId { get; set; }

        [Display(Name = "Weight")]
        public double Weight { get; set; }

        public virtual Assignment Assignment { get; set; }

        public virtual Item Item { get; set; }
    }
}