﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("UserDevice")]
    public class UserDevice : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserDeviceId { get; set; }

        [Display(Name = "Device")]
        public int DeviceId { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }

        [Display(Name = "Logged In")]
        public bool LoggedIn { get; set; }

        public string Lat { get; set; }

        public string Lan { get; set; }

        public virtual User User { get; set; }
        public virtual Device Device { get; set; }
    }
}