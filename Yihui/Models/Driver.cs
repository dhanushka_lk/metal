﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    public class Driver
    {
        public string Name { get; set; }

        public string ContactNo { get; set; }

        public string VehcileNo { get; set; }

        public string JobName { get; set; }

        public string CompanyName { get; set; }

        public string Lat { get; set; }

        public string Lan { get; set; }
    }
}