﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Yihui.Models
{
    [Table("AssignmentTransfer")]
    public class AssignmentTransfer:BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int TransferId { get; set; }

        [Display(Name = "Old Owner")]
        public Nullable<int> OldOwnerUserId { get; set; }

        [Display(Name = "New Owner")]
        public int NewOwnerUserId { get; set; }

        [Display(Name = "TransferDate")]
        public DateTime TransferDate { get; set; }

        public virtual User NewOwner { get; set; }

        public virtual User OldOwner { get; set; }
    }
}